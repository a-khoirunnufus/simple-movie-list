import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const SearchForm = ({classes, handleSearch, handleChange, mainState}) => {
	return (
	<>
	<Typography variant="h4">Search A Movie</Typography>
	<Grid container direction="column" spacing={1} className={classes.form} alignItems="center">
		<Grid xs={10} item>
			<TextField name="title" id="movie-title" label="Movie Title" placeholder="type a movie title" variant="outlined" onChange={handleChange}/>
		</Grid>
		<Grid xs={10} item>
			<TextField name="type" id="movie-type" label="Movie Type" placeholder="type a movie type" variant="outlined" onChange={handleChange}/>
		</Grid>
		<Grid xs={10} item>
			<TextField name="year" id="movie-year" label="Movie Year" placeholder="type a movie year" variant="outlined" onChange={handleChange}/>
		</Grid>
		<Grid xs={10} style={{marginTop:20}} item>
			<Button variant="contained" color="primary" onClick={handleSearch}>
				Search
			</Button>
		</Grid>		
	</Grid>
	{mainState.loadingResult && <Typography variant="subtitle1">Loading ...</Typography>}
	</>
	);
}

export default SearchForm;