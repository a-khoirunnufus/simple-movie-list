import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {Card, CardMedia, CardContent, CardActionArea} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const SearchResult = ({mainState, classes, handlePage}) => {
	return (	
		<React.Fragment>
		<Typography variant="h4">Search Result</Typography>
		{mainState.movies.Response === "True" ?
		<>
		<Grid container style={{marginTop:40}} spacing={2} justify="center">
			{mainState.movies.Search.map((item,index) => (
			<Grid xs={3} key={index} item>
				<Card className={classes.card}>
					<CardActionArea>
						<CardMedia
						  className={classes.cardMedia}
						  image={item.Poster}
						  title={item.Title}
						/>
						<CardContent>
							<Grid container direction="column" justify="center">
								<Typography variant="caption">
									{item.Title+' ('+item.Year+')'}
								</Typography>
								<Typography variant="overline" className={classes.textSecondary}>
									{item.Type}
								</Typography>
							</Grid>
						</CardContent>
					</CardActionArea>
				</Card>
			</Grid>
			))}
		</Grid>
		<Grid container justify="center" style={{marginTop:40}}>
			<PageNumberBtn length={mainState.totalPages} handlePage={handlePage} pageNumber={mainState.pageNumber} />
		</Grid>
		</>
		:<Typography variant="body1" style={{marginTop:20}}>Movies Not Found</Typography>}
		<div style={{marginTop:70}} />
		</React.Fragment>
	);
}

const PageNumberBtn = (props) => {
	const items = [];

	for (let i = 1; i<=props.length; i++){
		items.push(
			<Button key={i} onClick={props.handlePage} size="small" variant={i === parseInt(props.pageNumber)?"contained":"outlined"} color="primary" style={{marginLeft:5, marginRight:5}}>
				{i}
			</Button>
		)
	}
	return (
		<React.Fragment>{items}</React.Fragment>
	)
}

export default SearchResult;