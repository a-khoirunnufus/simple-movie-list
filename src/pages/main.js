import React,{useState} from 'react';
import SearchForm from '../components/searchform.js';
import SearchResult from '../components/searchresult.js';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {useSpring, animated} from 'react-spring';

const defaultMovies = {
	Response: "",
	Search:[],
	totalResults: ""
};

const useStyles = makeStyles(theme => ({
	root: {
		marginTop:70,
		textAlign: 'center',
		position: 'relative'
	},
	form: {
		marginTop:40,
		marginBottom:40
	},
	card: {
		maxWidth: '100%',
		height: 337
	},
	cardMedia: {
		height: 255,
	},
	textSecondary: {
		color: theme.palette.text.secondary
	}
}));

export default function Main() {
	const [formData, setFormData] = useState({
		title: '',
		type: '',
		year: '',
		page: 1
	});
	const [movies, setMovies] = useState(defaultMovies);
	const [pageNumber, setPageNumber] = useState(1);
	const [totalPages, setTotalPages] = useState(0);
	const [loadingResult, setLoadingResult] = useState(false);
	const [showResult, setShowResult] = useState(false);
	const [swipe, setSwipe] = useState(false);
	const classes = useStyles();
	const searchAnim = useSpring({
		to: swipe ? {left: '5%', marginLeft: '0px'} : {left: '50%', marginLeft: '-160px'}
	});
	const resultAnim = useSpring({
		opacity: showResult ? 1 : 0
	});

	//fetch data
	const getMovie = async (title,type,year,page) => {
		setShowResult(false);
		setLoadingResult(true);
		let url = 'https://www.omdbapi.com/?apikey=c404018b&s='+title;
		if (type) url = url+'&type='+type;
		if (year) url = url+'&y='+year;
		url = url+'&page='+page;

		const response = await fetch(url);
		if(response.status !== 200){
			throw new Error('cannot fetch the data');
		}
		const data = await response.json();

		setMovies(data);
		setTotalPages(Math.ceil(parseInt(data.totalResults) / 10));
		setLoadingResult(false);
		setShowResult(true);
	};

	//handle type input change
	const handleChange = (event) => {
		setFormData({
			...formData,
			[event.target.name]: event.target.value
		})
	};

	//handle button search
	const handleSearch = () => {
		setSwipe(true);
		getMovie(formData.title,formData.type,formData.year,formData.page);
	};

	//handle pergantian page
	const handlePage = (event) => {
		let num = event.target.innerText;
		setPageNumber(num);
		console.log(event);
		getMovie(formData.title,formData.type,formData.year,num);
	};

	return(
		<Container maxWidth="lg" className={classes.root} disableGutters>
			<Grid container direction="row" justify="center">
				<animated.div className='search-form' style={searchAnim}>
					<SearchForm classes={classes} handleSearch={handleSearch} handleChange={handleChange} mainState={{loadingResult}} />
				</animated.div>
				<animated.div className='search-result' style={resultAnim}>
					<SearchResult mainState={{movies, totalPages, pageNumber}} classes={classes} handlePage={handlePage} />
				</animated.div>
			</Grid>
		</Container>
	);
}

